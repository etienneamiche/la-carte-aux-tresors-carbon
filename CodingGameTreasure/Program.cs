﻿namespace CodingGameTreasure
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using CodingGameTreasure.Model;
    using CodingGameTreasure.Utils;

    internal class Program
    {
        private async static Task Main(string[] args)
        {
            try
            {
                if (args.Length != 2 || !File.Exists(args[0]))
                {
                    throw new FieldAccessException("Erreur dans les chemins d'accès");
                }

                GameInputFileReader gifr = new();
                Game g = gifr.ReadGameInput(args[0]);
                g.Execute();
                GameOutputFileWriter gofw = new();
                await gofw.Write(g, args[1]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    } 
}
