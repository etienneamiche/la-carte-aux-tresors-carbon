﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<En attente>")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "<En attente>")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<En attente>", Scope = "member", Target = "~M:CodingGameTreasure.Model.Game_Concept.Coordinates.#ctor(System.Int32,System.Int32)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<En attente>", Scope = "member", Target = "~M:CodingGameTreasure.Model.Game_Concept.Position.#ctor(CodingGameTreasure.Model.Game_Concept.Coordinates,CodingGameTreasure.Model.Game_Concept.Direction)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<En attente>", Scope = "member", Target = "~M:CodingGameTreasure.Model.Map.#ctor(System.Int32,System.Int32)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<En attente>", Scope = "member", Target = "~M:CodingGameTreasure.Model.Map_Elements.Adventurer.#ctor(System.String,System.Int32,System.Int32,CodingGameTreasure.Model.Game_Concept.Direction,System.String)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<En attente>", Scope = "member", Target = "~M:CodingGameTreasure.Utils.GameInputFileReader.#ctor")]
[assembly: SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1000:Keywords should be spaced correctly", Justification = "<En attente>", Scope = "member", Target = "~M:CodingGameTreasure.Utils.GameInputFileReader.ExtractAdventurerInfo(System.String)~CodingGameTreasure.Model.Map_Elements.Adventurer")]
[assembly: SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1000:Keywords should be spaced correctly", Justification = "<En attente>", Scope = "member", Target = "~M:CodingGameTreasure.Utils.GameInputFileReader.RegExMatcher(System.String,System.String)~System.Boolean")]
