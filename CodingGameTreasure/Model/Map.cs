﻿namespace CodingGameTreasure.Model
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Map
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public Map(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public Map()
        {
        }

        public string ToLineString()
        {
            return $"C - {this.Width} - {this.Height}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Map m = (Map)obj;
                return this.Width == m.Width && this.Height == m.Height;
            }
        }
    }
}
