﻿namespace CodingGameTreasure.Model.Map_Elements
{
    using System;
    using System.Collections.Generic;
    using CodingGameTreasure.Model.Game_Concept;
    using CodingGameTreasure.Model.Map_Elements.Concept;

    public class Adventurer : Movable
    {
        public string Name { get; set; }

        public Stack<char> Sequence { get; set; }

        public int Treasures { get; set; }

        public Adventurer(string name, int x, int y, Direction direction, string sequence)
        {
            this.Name = name;
            this.Coordinates = new Coordinates(x, y);
            this.Direction = direction;

            this.Sequence = new();
            char[] charSequence = sequence.ToCharArray();
            Array.Reverse(charSequence);
            Array.ForEach(charSequence, c => this.Sequence.Push(c));

            this.Treasures = 0;
        }

        public bool TreasurePresent(Dictionary<Coordinates, Treasure> treasures)
        {
            return treasures.ContainsKey(this.Coordinates);
        }

        public bool IsAdventurerAhead(Coordinates ahead, Dictionary<int, Adventurer> adventurers)
        {
            foreach (var a in adventurers)
            {
                if (a.Value.Coordinates.Equals(ahead)) return true;
            }

            return false;
        }

        public bool IsMountainAhead(Coordinates ahead, Dictionary<Coordinates, Mountain> mountains)
        {
            return mountains.ContainsKey(ahead);
        }

        public string ToLineString()
        {
            return $"A - {this.Name} - {this.Coordinates.X} - {this.Coordinates.Y} - {this.Direction.ToString("F")} - {this.Treasures}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Adventurer a = (Adventurer)obj;
                return this.Name == a.Name && this.IsSameSequence(a.Sequence) && this.Coordinates.Equals(a.Coordinates);
            }
        }

        private bool IsSameSequence(Stack<char> stack2)
        {
            bool flag = true;

            if (this.Sequence.Count != stack2.Count)
            {
                flag = false;
                return flag;
            }

            while (this.Sequence.Count != 0)
            {
                if (this.Sequence.Peek() == stack2.Peek())
                {
                    this.Sequence.Pop();
                    stack2.Pop();
                }
                else
                {
                    return false;
                }
            }

            return flag;
        }
    }
}
