﻿namespace CodingGameTreasure.Model.Map_Elements.Concept
{
    using System;
    using CodingGameTreasure.Model.Game_Concept;

    public abstract class Movable : MapElement
    {
        public Direction Direction { get; set; }
    }
}