﻿namespace CodingGameTreasure.Model.Map_Elements.Concept
{
    using CodingGameTreasure.Model.Game_Concept;

    public class MapElement
    {
        public Coordinates Coordinates { get; set; }
    }
}
