﻿namespace CodingGameTreasure.Model.Map_Elements.Concept
{
    public class Consumable : MapElement
    {
        public int Amount { get; set; }
    }
}