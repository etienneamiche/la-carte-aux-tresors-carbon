﻿namespace CodingGameTreasure.Model.Map_Elements
{
    using CodingGameTreasure.Model.Game_Concept;
    using CodingGameTreasure.Model.Map_Elements.Concept;

    public class Mountain : Obstacle
    {
        public Mountain(int x, int y)
        {
            this.Coordinates = new Coordinates(x, y);
        }

        public string ToLineString()
        {
            return $"M - {this.Coordinates.X} - {this.Coordinates.Y}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Mountain a = (Mountain)obj;
                return this.Coordinates.Equals(a.Coordinates);
            }
        }
    }
}
