﻿namespace CodingGameTreasure.Model.Map_Elements
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CodingGameTreasure.Model.Game_Concept;
    using CodingGameTreasure.Model.Map_Elements.Concept;

    public class Treasure : Consumable
    {
        public Treasure(int x, int y, int amount)
        {
            this.Coordinates = new Coordinates(x, y);
            this.Amount = amount;
        }

        public int Loot()
        {
            this.Amount--;
            return this.Amount;
        }

        public string ToLineString()
            {
                return $"T - {this.Coordinates.X} - {this.Coordinates.X} - {this.Amount}";
            }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Treasure a = (Treasure)obj;
                return this.Coordinates.Equals(a.Coordinates);
            }
        }
    }
}
