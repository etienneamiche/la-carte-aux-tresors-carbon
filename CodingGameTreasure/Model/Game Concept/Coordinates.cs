﻿namespace CodingGameTreasure.Model.Game_Concept
{
    using System;

    public class Coordinates
    {
        public int X { get; set; }

        public int Y { get; set; }

        public Coordinates(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Coordinates Forward(Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    return new Coordinates(this.X, this.Y - 1);
                case Direction.E:
                    return new Coordinates(this.X + 1, this.Y);
                case Direction.W:
                    return new Coordinates(this.X - 1, this.Y);
                case Direction.S:
                    return new Coordinates(this.X, this.Y + 1);
                default: return this;
            }
        }

        public bool IsInBound(Map map)
        {
            return
                this.X >= 0 &&
                this.X < map.Width &&
                this.Y >= 0 &&
                this.Y < map.Height;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Coordinates c = (Coordinates)obj;
                return (this.X == c.X) && (this.Y == c.Y);
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.X, this.Y);
        }
    }
}
