﻿namespace CodingGameTreasure.Model.Game_Concept
{
    public enum Direction
    {
        N,
        E,
        S,
        W,
    }

    internal static class ExtensionDirection
    {
        public static Direction TurnLeft(this Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    return Direction.W;
                case Direction.E:
                    return Direction.N;
                case Direction.W:
                    return Direction.S;
                case Direction.S:
                    return Direction.E;
                default: return direction;
            }
        }

        public static Direction TurnRight(this Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    return Direction.E;
                case Direction.E:
                    return Direction.S;
                case Direction.W:
                    return Direction.N;
                case Direction.S:
                    return Direction.W;
                default: return direction;
            }
        }
    }
}
