﻿using CodingGameTreasure.Controller;
using CodingGameTreasure.Model.Game_Concept;
using CodingGameTreasure.Model.Map_Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingGameTreasure.Model
{
    public class Game
    {
        public Map Map { get; set; }

        public Dictionary<int, Adventurer> Adventurers { get; set; }

        public Dictionary<Coordinates, Mountain> Mountains { get; set; }

        public Dictionary<Coordinates, Treasure> Treasures { get; set; }

        public AdventurerController AdventurerController { get; set; }

        public TreasureController TreasureController { get; set; }

        public Game(Map map, Dictionary<int, Adventurer> adventurers, Dictionary<Coordinates, Mountain> mountains, Dictionary<Coordinates, Treasure> treasures)
        {
            this.Map = map;
            this.Adventurers = adventurers;
            this.Mountains = mountains;
            this.Treasures = treasures;
            this.AdventurerController = new();
            this.TreasureController = new();
        }

        public void Execute()
        {
            int numberOfAdventurer = this.Adventurers.Count;
            bool[] adventurersHaveMoved = new bool[numberOfAdventurer];

            while (!adventurersHaveMoved.All(x => x))
            {
                for (int order = 0; order < numberOfAdventurer; order++)
                {
                    if (!adventurersHaveMoved[order])
                    {
                        adventurersHaveMoved[order] = this.AdventurerController.Move(this.Adventurers[order], this);
                    }
                }
            }
        }
    }
}
