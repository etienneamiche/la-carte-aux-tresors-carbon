﻿namespace CodingGameTreasure.Utils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using CodingGameTreasure.Model;
    using CodingGameTreasure.Model.Game_Concept;
    using CodingGameTreasure.Model.Map_Elements;
    using CodingGameTreasure.Model.Map_Elements.Concept;
    using CodingGameTreasure.Utils.Constants;
    using CodingGameTreasure.Utils.CustomException;

    // Exemple :
    //  # {C comme Carte} - {Nb. de case en largeur} - {Nb. de case en hauteur}
    //  C - 3 - 4
    //  # {M comme Montagne} - {Axe horizontal} - {Axe vertical}
    //  M - 1 - 0
    //  M - 2 - 1
    //  # {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors}
    //  T - 0 - 3 - 2
    //  T - 1 - 3 - 3
    //  # {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement}
    //  A - Lara - 1 - 1 - S - AADADAGGA
    public class GameInputFileReader
    {
        public Map Map { get; set; }

        public Dictionary<int, Adventurer> Adventurers { get; set; }

        public int AdventurerIndex { get; set; }

        public Dictionary<Coordinates, Mountain> Mountains { get; set; }

        public Dictionary<Coordinates, Treasure> Treasures { get; set; }

        private Regex Rgx { get; set; }

        public GameInputFileReader()
        {
            this.Map = null;
            this.Adventurers = new();
            this.AdventurerIndex = 0;
            this.Mountains = new();
            this.Treasures = new();
        }

        public Game ReadGameInput(string path)
        {
            try
            {
                using (var sr = new StreamReader(path))
                {
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        this.ExtractLineInfo(line);
                    }

                    return new Game(this.Map, this.Adventurers, this.Mountains, this.Treasures);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public bool ExtractLineInfo(string line)
        {
            if (this.RegExMatcher(RegExPaterns.Adventurer, line))
            {
                this.ExtractAdventurerInfo(line);
            }
            else if (this.RegExMatcher(RegExPaterns.Map, line))
            {
                this.ExtractMapInfo(line);
            }
            else if (this.RegExMatcher(RegExPaterns.Treasure, line))
            {
                this.ExtractTreasureInfo(line);
            }
            else if (this.RegExMatcher(RegExPaterns.Mountain, line))
            {
                this.ExtractMountainInfo(line);
            }
            else if (this.RegExMatcher(RegExPaterns.Comment, line))
            {
                // Skip
            }
            else
            {
                throw new BadGameInputFormat(line);
            }

            return true;
        }

        public bool RegExMatcher(string patern, string line)
        {
            this.Rgx = new(patern);
            return this.Rgx.IsMatch(line);
        }

        public bool IsInbound(MapElement go)
        {
            if (this.Map is not null)
            {
                return (go.Coordinates.X >= 0 && go.Coordinates.X < this.Map.Width) && (go.Coordinates.Y >= 0 && go.Coordinates.Y < this.Map.Height);
            }
            else
            {
                return false;
            }
        }

        public Adventurer ExtractAdventurerInfo(string line)
        {
            string[] adventurer = line.Split('-');
            Adventurer a = new(
                adventurer[1].Trim(),
                int.Parse(adventurer[2].Trim()),
                int.Parse(adventurer[3].Trim()),
                (Direction)Enum.Parse(typeof(Direction), adventurer[4].Trim()),
                adventurer[5].Trim());
            if (!this.Adventurers.ContainsValue(a) && this.IsInbound(a))
            {
                this.Adventurers.Add(this.AdventurerIndex, a);
                this.AdventurerIndex++;
                return a;
            }
            else
            {
                throw new BadGameInputFormat(line);
            }
        }

        public Mountain ExtractMountainInfo(string line)
        {
            string[] mountain = line.Split('-');
            Mountain m = new(
                int.Parse(mountain[1].Trim()),
                int.Parse(mountain[2].Trim()));
            if (!this.Mountains.ContainsKey(m.Coordinates) && this.IsInbound(m))
            {
                this.Mountains.Add(m.Coordinates, m);
                return m;
            }
            else
            {
                throw new BadGameInputFormat(line);
            }
        }

        public Treasure ExtractTreasureInfo(string line)
        {
            string[] treasure = line.Split('-');
            Treasure t = new(
                int.Parse(treasure[1].Trim()),
                int.Parse(treasure[2].Trim()),
                int.Parse(treasure[3].Trim()));
            if (!this.Treasures.ContainsKey(t.Coordinates) && this.IsInbound(t))
            {
                this.Treasures.Add(t.Coordinates, t);
                return t;
            }
            else
            {
                throw new BadGameInputFormat(line);
            }
        }

        public Map ExtractMapInfo(string line)
        {
            string[] map = line.Split('-');
            Map m = new(
                int.Parse(map[1].Trim()),
                int.Parse(map[2].Trim()));
            if (this.Map is null)
            {
                this.Map = m;
                return m;
            }
            else
            {
                throw new BadGameInputFormat(line);
            }
        }
    }
}
