﻿using CodingGameTreasure.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingGameTreasure.Utils
{
    public class GameOutputFileWriter
    {
        public GameOutputFileWriter() { }

        public async Task Write(Game game, string path)
        {

            using (StreamWriter sw = File.CreateText(path))
            {
                await sw.WriteLineAsync(game.Map.ToLineString());

                foreach (var mountain in game.Mountains)
                {
                    await sw.WriteLineAsync(mountain.Value.ToLineString());
                }

                foreach (var treasure in game.Treasures)
                {
                    await sw.WriteLineAsync(treasure.Value.ToLineString());
                }

                foreach (var adventurer in game.Adventurers)
                {
                    await sw.WriteLineAsync(adventurer.Value.ToLineString());
                }
            }
        }
    }
}
