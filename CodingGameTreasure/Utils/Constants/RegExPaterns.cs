﻿namespace CodingGameTreasure.Utils.Constants
{
    public static class RegExPaterns
    {
        public const string Map = @"^C - [0-9]+ - [0-9]+$";
        public const string Adventurer = @"^A - \w+ - [0-9]+ - [0-9]+ - [NESW] - [ADG]+$";
        public const string Treasure = @"^T - [0-9]+ - [0-9]+ - [0-9]+$";
        public const string Mountain = @"^M - [0-9]+ - [0-9]+$";
        public const string Comment = @"^#.+";
    }
}
