﻿namespace CodingGameTreasure.Utils.Constants
{
    public class MoveSequenceAction
    {
        public const char Forward = 'A';
        public const char TurnLeft = 'G';
        public const char TurnRight = 'D';
    }
}
