# La Carte Aux Trésors Carbon

## Utilisation
Le programme prend deux paramètres en entrée : 
- Chemin du fichier d'entrée
- Chemin et non du fichier de sortie qui sera généré

### Le fichier d'entrée doit respecter les contraintes suivantes : 

- Ligne carte en première entrée
Les aventuriers, les montagnes et les trésors dans n’importe quel ordre.

- Pas de superposition d'éléments de même type

### Le fichier de sortie est formaté de la manière suivante
- Premiere ligne Carte 
- Puis Montagne.s
- Puis Trésor.s
- Enfin Aventurier.s
