﻿namespace CodingGameTreasureTest
{
    using System.Collections.Generic;
    using CodingGameTreasure.Controller;
    using CodingGameTreasure.Model;
    using CodingGameTreasure.Model.Game_Concept;
    using CodingGameTreasure.Model.Map_Elements;
    using Xunit;

    public class AdventurerTest
    {
        private Adventurer A = new("Gates", 0, 0, Direction.S, "A");
        private AdventurerController ac = new();

        [Fact]
        public void ForwardTest()
        {
            Map m = new(3, 3);
            Mountain mo = new(0, 1);
            Dictionary<Coordinates, Mountain> mountains = new();
            mountains.Add(mo.Coordinates,mo);

            Adventurer B = new("Bloc",1, 1, Direction.S, "G");
            Dictionary<int, Adventurer> adventurers = new();
            adventurers.Add(0,B);
            adventurers.Add(1, this.A);

            Treasure t = new(2, 1, 1);
            Dictionary<Coordinates, Treasure> treasures = new();
            treasures.Add(t.Coordinates,t);

            Game G = new(m, adventurers, mountains, treasures);

            Assert.True(G.Adventurers[1].IsMountainAhead(G.Adventurers[1].Coordinates.Forward(G.Adventurers[1].Direction),G.Mountains));
            this.ac.Forward(G, G.Adventurers[1]);

            this.ac.TurnLeft(G.Adventurers[1]);
            this.ac.Forward(G, G.Adventurers[1]);
            this.ac.TurnRight(G.Adventurers[1]);

            Assert.True(G.Adventurers[1].IsAdventurerAhead(G.Adventurers[1].Coordinates.Forward(G.Adventurers[1].Direction), G.Adventurers));
            this.ac.Forward(G, G.Adventurers[1]);
            this.ac.TurnLeft(G.Adventurers[1]);
            this.ac.Forward(G, G.Adventurers[1]);
            this.ac.TurnRight(G.Adventurers[1]);

            this.ac.Forward(G, G.Adventurers[1]);
            this.ac.Forward(G, G.Adventurers[1]);




            Assert.True(adventurers[1].Coordinates.Equals(new Coordinates(2,2)) && adventurers[1].Treasures == 1 && G.Treasures.Count == 0);
        }

        [Theory]
        [InlineData(Direction.N, Direction.W)]
        [InlineData(Direction.W, Direction.S)]
        [InlineData(Direction.S, Direction.E)]
        [InlineData(Direction.E, Direction.N)]
        public void TurnLeftTest(Direction initial, Direction expected)
        {
            this.A.Direction = initial;
            this.ac.TurnLeft(this.A);
            Assert.True(this.A.Direction == expected);
        }

        [Theory]
        [InlineData(Direction.N, Direction.E)]
        [InlineData(Direction.E, Direction.S)]
        [InlineData(Direction.S, Direction.W)]
        [InlineData(Direction.W, Direction.N)]
        public void TurnRightTest(Direction initial, Direction expected)
        {
            this.A.Direction = initial;
            this.ac.TurnRight(this.A);
            Assert.True(this.A.Direction == expected);
        }
    }
}
