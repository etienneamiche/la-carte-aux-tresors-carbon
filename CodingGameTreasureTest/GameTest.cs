﻿namespace CodingGameTreasureTest
{
    using CodingGameTreasure.Model;
    using CodingGameTreasure.Model.Game_Concept;
    using CodingGameTreasure.Model.Map_Elements;
    using CodingGameTreasure.Utils;
    using Xunit;

    public class GameTest
    {
        [Fact]
        public void ExecuteTest()
        {
            Game G = new GameInputFileReader().ReadGameInput("C:\\Users\\etien\\source\\repos\\CodingGameTreasure\\CodingGameTreasure\\TextFile.txt");
            G.Execute();
            Adventurer Lara = G.Adventurers[0];
            Assert.True(Lara.Coordinates.X == 0 && Lara.Coordinates.Y == 3);
            Assert.True(Lara.Direction == CodingGameTreasure.Model.Game_Concept.Direction.S);
            Assert.True(Lara.Treasures == 3);
            Assert.True(G.Treasures.ContainsKey(new Coordinates(1, 3)));
            Assert.False(G.Treasures.ContainsKey(new Coordinates(0, 3)));
            Assert.True(G.Treasures[new Coordinates(1, 3)].Amount == 2);
        }
    }
}
