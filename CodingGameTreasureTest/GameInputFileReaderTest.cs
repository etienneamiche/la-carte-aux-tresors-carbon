namespace CodingGameTreasureTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CodingGameTreasure.Model;
    using CodingGameTreasure.Model.Game_Concept;
    using CodingGameTreasure.Model.Map_Elements;
    using CodingGameTreasure.Model.Map_Elements.Concept;
    using CodingGameTreasure.Utils;
    using CodingGameTreasure.Utils.Constants;
    using CodingGameTreasure.Utils.CustomException;
    using CodingGameTreasureTest.Utils;
    using Xunit;

    public class GameInputFileReaderTest
    {
        [Theory]
        [InlineData("M - 1 - 0", true)]
        [InlineData("M - -1 - 0", false)]
        [InlineData("T - 1 - 3 - 3", true)]
        [InlineData("T - 1 - -3 - 3", false)]
        [InlineData("#T - 1 - 3 - 3", true)]
        [InlineData("A - Lara - 1 - 1 - S - AADADAGGA", true)]
        public void ExtractLineInfoTest(string line, bool result)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(6, 6);
            if (result)
            {
                Assert.True(gifr.ExtractLineInfo(line));
            }
            else
            {
                Action act = () => gifr.ExtractLineInfo(line);
                Assert.Throws<BadGameInputFormat>(act);
            }
        }

        [Theory]
        [InlineData(3, 5, 2, 2, true)]
        [InlineData(3, 5, 1, 6, false)]
        [InlineData(3, 5, 3, 5, false)]
        [InlineData(3, 5, 2, 5, false)]
        [InlineData(3, 5, 3, 4, false)]
        [InlineData(23, 45, -1, 0, false)]
        [InlineData(23, 45, 0, 0, true)]
        [InlineData(23, 45, 24, 47, false)]
        public void IsInboundTest(int width, int height, int x, int y, bool result)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(width, height);
            MapElement go = new();
            go.Coordinates = new(x, y);
            Assert.True(gifr.IsInbound(go) == result);
        }

        [Theory]
        [InlineData("A - Lara - 1 - 1 - S - AADADAGGA", "Lara", 1, 1, Direction.S, "AADADAGGA", true)]
        public void ExtractAdventurerInfoTest(string line, string name, int x, int y, Direction direction, string sequence, bool expected)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(6, 6);
            Adventurer adventurerExtracted = gifr.ExtractAdventurerInfo(line);
            Adventurer adventurerTheory = new(name, x, y, direction, sequence);
            Assert.True(adventurerExtracted.Equals(adventurerTheory) == expected);
        }

        [Theory]
        [InlineData("A - Lara - 1 - 1 - S - AADADAGGA", "Lara", 1, 1, Direction.S, "AADADAGGA")]
        public void ExtractAdventurerInfoExceptionTest(string line, string name, int x, int y, Direction direction, string sequence)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(6, 6);
            Adventurer adventurerTheory = new(name, x, y, direction, sequence);
            gifr.Adventurers.Add(0, adventurerTheory);
            gifr.AdventurerIndex++;
            Action act = () => gifr.ExtractAdventurerInfo(line);
            Assert.Throws<BadGameInputFormat>(act);
        }

        [Theory]
        [InlineData("M - 1 - 0", 1, 0, true)]
        public void ExtractMountainInfoTest(string line, int x, int y, bool expected)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(6, 6);
            Mountain mountainExtracted = gifr.ExtractMountainInfo(line);
            Mountain mountainTheory = new(x, y);
            Assert.True(mountainExtracted.Equals(mountainTheory) == expected);
        }

        [Theory]
        [InlineData("M - 1 - 0", 1, 0)]
        public void ExtractMountainInfoExceptionTest(string line, int x, int y)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(6, 6);
            Mountain mountainTheory = new(x, y);
            gifr.Mountains.Add(mountainTheory.Coordinates, mountainTheory);
            Action act = () => gifr.ExtractMountainInfo(line);
            Assert.Throws<BadGameInputFormat>(act);
        }

        [Theory]
        [InlineData("T - 1 - 3 - 3", 1, 3, 3, true)]
        public void ExtractTreasureInfoTest(string line, int x, int y, int amount, bool expected)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(6, 6);
            Treasure treasureExtracted = gifr.ExtractTreasureInfo(line);
            Treasure treasureTheory = new(x, y, amount);
            Assert.True(treasureExtracted.Equals(treasureTheory) == expected);
        }

        [Theory]
        [InlineData("T - 1 - 3 - 3", 1, 3, 3)]
        public void ExtractTreasureInfoExceptionTest(string line, int x, int y, int amout)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(6, 6);
            Treasure tresureTheory = new(x, y, amout);
            gifr.Treasures.Add(tresureTheory.Coordinates, tresureTheory);
            Action act = () => gifr.ExtractTreasureInfo(line);
            Assert.Throws<BadGameInputFormat>(act);
        }

        [Theory]
        [InlineData("C - 3 - 4", 3, 4, true)]
        public void ExtractMapInfoTest(string line, int width, int height, bool result)
        {
            GameInputFileReader gifr = new();
            Map mapExtracted = gifr.ExtractMapInfo(line);
            Map mapTheory = new(width, height);
            Assert.True(mapExtracted.Equals(mapTheory) == result);
        }

        [Theory]
        [InlineData("C - 3 - 4", 3, 4)]
        public void ExtractMapInfoExceptionTest(string line, int width, int height)
        {
            GameInputFileReader gifr = new();
            gifr.Map = new(width, height);
            Action act = () => gifr.ExtractMapInfo(line);
            Assert.Throws<BadGameInputFormat>(act);
        }

        [Theory]
        [InlineData("C - 3 - 4", RegExPaterns.Map, true)]
        [InlineData("C - -3 - 4", RegExPaterns.Map, false)]
        [InlineData("T - 1 - 3 - 3", RegExPaterns.Treasure, true)]
        [InlineData("T - 1 - -3 - 3", RegExPaterns.Treasure, false)]
        [InlineData("M - 1 - 0", RegExPaterns.Mountain, true)]
        [InlineData("#M - 1 - 0", RegExPaterns.Mountain, false)]
        [InlineData("A - Lara - 1 - 1 - S - AADADAGGA", RegExPaterns.Adventurer, true)]
        [InlineData("A - Lara - 1 - 1 - F - AADADAGGA", RegExPaterns.Adventurer, false)]
        [InlineData("# C - 3 - 4 dzqd", RegExPaterns.Comment, true)]
        [InlineData("B - Lara - 1 - 1 - S - AADADAGGA", RegExPaterns.Adventurer, false)]
        [InlineData("qzd GA", RegExPaterns.Map, false)]
        public void RegExMatcherTest(string line, string patern, bool result)
        {
            GameInputFileReader gifr = new();
            Assert.True(gifr.RegExMatcher(patern, line) == result);
        }

        [Fact]
        public void ReadGameInputTestTrue()
        {
            GameInputFileReader gifr = new();
            Game g = gifr.ReadGameInput("C:\\Users\\etien\\source\\repos\\CodingGameTreasure\\CodingGameTreasure\\TextFile.txt");

            // Adventurer
            Adventurer a = new("Lara", 1, 1, Direction.S, "AADADAGGA");
            Dictionary<int, Adventurer> aL = new();
            aL.Add(0, a);

            // Map
            Map m = new(3, 4);

            // Mountains
            Mountain m1 = new(1, 0);
            Mountain m2 = new(2, 1);
            Dictionary<Coordinates, Mountain> mL = new();
            mL.Add(m1.Coordinates, m1);
            mL.Add(m2.Coordinates, m2);

            // Treasures
            Treasure t1 = new(0, 3, 2);
            Treasure t2 = new(1, 3, 3);
            Dictionary<Coordinates, Treasure> tL = new();
            tL.Add(t1.Coordinates, t1);
            tL.Add(t2.Coordinates, t2);

            // Game
            Game gTheory = new(m, aL, mL, tL);

            Assert.True(g is not null);
            Assert.True(g.Map.Equals(gTheory.Map));

            bool Mountainequal = Compare.MountainsEquals(g, gTheory);
            Assert.True(Mountainequal);
            bool TreasureEqual = Compare.TreasuresEquals(g, gTheory);

            Assert.True(TreasureEqual);
            Assert.True(g.Adventurers[0].Equals(gTheory.Adventurers[0]));
        }
    }
}
