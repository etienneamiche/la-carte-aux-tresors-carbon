﻿namespace CodingGameTreasureTest
{
    using CodingGameTreasure.Model.Game_Concept;
    using Xunit;

    public class CoordinateTest
    {
        [Theory]
        [InlineData(Direction.N, 6, 4, 6, 3)]
        [InlineData(Direction.E, 6, 4, 7, 4)]
        [InlineData(Direction.S, 6, 4, 6, 5)]
        [InlineData(Direction.W, 6, 4, 5, 4)]
        [InlineData(Direction.N, 3, 0, 3, -1)]
        [InlineData(Direction.W, 0, 3, -1, 3)]

        public void ForwardTest(Direction direction, int X, int Y, int expectedX, int expectedY)
        {
            Coordinates c = new(X, Y);
            c = c.Forward(direction);
            Coordinates expectedCoord = new Coordinates(expectedX, expectedY);
            Assert.True(c.Equals(expectedCoord));
        }
    }
}
