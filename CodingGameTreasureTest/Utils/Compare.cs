﻿namespace CodingGameTreasureTest.Utils
{
    using CodingGameTreasure.Model;
    using CodingGameTreasure.Model.Map_Elements;

    public static class Compare
    {
        public static bool TreasuresEquals(Game g, Game gTheory)
        {
            bool treasureEqual = false;
            if (g.Treasures.Count == gTheory.Treasures.Count)
            {
                treasureEqual = true;
                foreach (var pair in g.Treasures)
                {
                    Treasure value;
                    if (gTheory.Treasures.TryGetValue(pair.Key, out value))
                    {
                        // Require value be equal.
                        if (!value.Equals(pair.Value))
                        {
                            treasureEqual = false;
                            break;
                        }
                    }
                    else
                    {
                        treasureEqual = false;
                        break;
                    }
                }
            }

            return treasureEqual;
        }

        public static bool MountainsEquals(Game g, Game gTheory)
        {
            bool mountainequal = false;
            if (g.Mountains.Count == gTheory.Mountains.Count)
            {
                mountainequal = true;
                foreach (var pair in g.Mountains)
                {
                    Mountain value;
                    if (gTheory.Mountains.TryGetValue(pair.Key, out value))
                    {
                        // Require value be equal.
                        if (!value.Equals(pair.Value))
                        {
                            mountainequal = false;
                            break;
                        }
                    }
                    else
                    {
                        mountainequal = false;
                        break;
                    }
                }
            }

            return mountainequal;
        }
    }
}
