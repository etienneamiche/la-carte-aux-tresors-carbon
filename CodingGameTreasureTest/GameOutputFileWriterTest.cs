﻿using CodingGameTreasure.Model;
using CodingGameTreasure.Model.Game_Concept;
using CodingGameTreasure.Model.Map_Elements;
using CodingGameTreasure.Utils;
using CodingGameTreasureTest.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CodingGameTreasureTest
{
    public class GameOutputFileWriterTest
    {
        [Fact]
        public void ToLineStringTest()
        {
            Map m = new(2, 4);
            Assert.True(m.ToLineString() == "C - 2 - 4");

            Adventurer a = new("Etienne", 1, 1, Direction.S, "AGDA");
            Assert.True(a.ToLineString().Equals("A - Etienne - 1 - 1 - S - 0"));

            Treasure t = new(2, 2, 3);
            Assert.True(t.ToLineString().Equals("T - 2 - 2 - 3"));

            Mountain mn = new(3, 5);
            Assert.True(mn.ToLineString().Equals("M - 3 - 5"));
        }

        [Theory]
        [InlineData(@"C:\Users\etien\Desktop\TestFiles\TEST1.txt", @"C:\Users\etien\Desktop\TestFiles\TEST1_RESULT.txt", @"C:\Users\etien\Desktop\TestFiles\TEST1_EXPECTED.txt")]
        [InlineData(@"C:\Users\etien\Desktop\TestFiles\TEST2.txt", @"C:\Users\etien\Desktop\TestFiles\TEST2_RESULT.txt", @"C:\Users\etien\Desktop\TestFiles\TEST2_EXPECTED.txt")]
        [InlineData(@"C:\Users\etien\Desktop\TestFiles\TEST3.txt", @"C:\Users\etien\Desktop\TestFiles\TEST3_RESULT.txt", @"C:\Users\etien\Desktop\TestFiles\TEST3_EXPECTED.txt")]
        public async void OutputTest(string input, string output, string expected)
        {
            GameInputFileReader gifr = new();
            Game g = gifr.ReadGameInput(input);
            g.Execute();
            GameOutputFileWriter gifw = new();
            await gifw.Write(g, output);
            Assert.True(FileCompare.Compare(output, expected));
        }
    }
}
